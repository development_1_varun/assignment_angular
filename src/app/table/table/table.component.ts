import { Component, OnInit } from '@angular/core';
import {data } from '../../Data.js'
@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {
  links:string[]=['Lab Alerts','Procedure Alerts','RX Specific Alerts','DX Specific Alerts','Patient Specific Alerts']
  tableRecords:Object[]=[...data]
  recordsOtherThanDelete=[]
  flagsForDeletion=[]
  indexes=[]
  constructor() { }

  ngOnInit() {
     this.flagsForDeletion=[...this.tableRecords]
     this.flagsForDeletion.fill(false)
  }
  


marked(i){
  this.indexes=[]
  this.flagsForDeletion[i]=!this.flagsForDeletion[i]
  this.flagsForDeletion.map((ele,index)=>{
if(ele==true){
this.indexes.push(index)
}
  })
}



delete(){
this.tableRecords.map((ele)=>{
  if(!this.indexes.includes(this.tableRecords.indexOf(ele))){
this.recordsOtherThanDelete.push(ele)
  }

})
this.tableRecords=[...this.recordsOtherThanDelete]
this.flagsForDeletion=[...this.tableRecords]
this.flagsForDeletion.fill(false)
this.recordsOtherThanDelete=[]
this.indexes=[]
}



onSubmit(newUserForm1){
  newUserForm1.value['id']=this.tableRecords.length+1
  console.log(newUserForm1.value)
  this.tableRecords.push(newUserForm1.value)
  newUserForm1.reset()
}

}
